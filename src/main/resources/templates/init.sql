drop database if exists sso;
create database sso;
use sso;
drop table if exists account;
create table account(
    id int primary key auto_increment,
    username varchar(20) not null,
    password varchar(200) not null,
    permission varchar(20) not null,
    account_non_expired tinyint(1) default true,
    account_non_locked tinyint(1) default true,
    credentials_non_expired tinyint(1) default true,
    enabled tinyint(1) default true
);
insert into account (username, password, permission) values ("123", "$2y$10$KaGG8mQC2pbeQXIBzWIDc.MScYxnpJVkvrgWvedSbkAtW2EzhlTES ", "ROLE_ADMIN");