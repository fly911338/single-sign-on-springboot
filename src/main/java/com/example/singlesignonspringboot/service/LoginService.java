package com.example.singlesignonspringboot.service;

import com.example.singlesignonspringboot.dao.UserRepository;
import com.example.singlesignonspringboot.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService {

    private final UserRepository repository;

    @Autowired
    public LoginService(UserRepository repository) {
        this.repository = repository;
    }

    public Optional<Account> findByUsername(String username) {
        return this.repository.findByUsername(username);
    }

    public void save(Account user) {
        this.repository.save(user);
    }

    public void del(Long userId) {
        Account user = this.repository.findById(userId).orElse(null);
        this.repository.delete(user);
    }
}
