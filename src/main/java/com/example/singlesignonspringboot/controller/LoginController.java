package com.example.singlesignonspringboot.controller;

import com.example.singlesignonspringboot.model.Account;
import com.example.singlesignonspringboot.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class LoginController {

    private final LoginService service;

    @Autowired
    public LoginController(LoginService service) {
        this.service = service;
    }

    @GetMapping("/get/{username}")
    public Account findByUsername(@PathVariable("username") String username) {
        return this.service.findByUsername(username).orElse(null);
    }

    @GetMapping("/save")
    public void save(Account user) {
        this.service.save(user);
    }

    @GetMapping("/del")
    public void del(Long userId) {
        this.service.del(userId);
    }

}
