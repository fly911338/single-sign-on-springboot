package com.example.singlesignonspringboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {

    @GetMapping("/busi")
    public String businessPage() {
        return "busi";
    }

    @GetMapping("/login")
    public String login() {return "login";}


}
