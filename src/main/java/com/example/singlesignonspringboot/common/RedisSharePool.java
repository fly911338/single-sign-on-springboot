package com.example.singlesignonspringboot.common;

import org.springframework.stereotype.Component;
import redis.clients.jedis.*;
import redis.clients.jedis.util.Hashing;

import java.util.ArrayList;

@Component
public class RedisSharePool {

    private ShardedJedisPool pool;

    private final Integer maxTotal = 20;

    private final Integer maxIdle = 20;

    private final Integer minIdle = 20;

//    private final Boolean testOnBorrow = true;
//
//    private final Boolean testOnReturn = true;

    private final String redis1Ip = "localhost";
    private final int redis1Port = 6379;

    private final String redis2Ip = "localhost";
    private final int redis2Port = 6380;

    private void initPool() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxIdle(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMinIdle(minIdle);
//        config.setTestOnBorrow(testOnBorrow);
//        config.setTestOnReturn(testOnReturn);
        config.setBlockWhenExhausted(true);
//        JedisShardInfo info1 = new JedisShardInfo(redis1Ip, redis1Port, 1000 * 2);
        JedisShardInfo info2 = new JedisShardInfo(redis2Ip, redis2Port, 1000 * 2);

        var jedisShardInfoList = new ArrayList<JedisShardInfo>(2);
//        jedisShardInfoList.add(info1);
        jedisShardInfoList.add(info2);

        pool = new ShardedJedisPool(config, jedisShardInfoList, Hashing.MURMUR_HASH);

    }

    public RedisSharePool() {
        initPool();
    }

    public ShardedJedis getJedis() { return pool.getResource();}

    public static void main(String[] args) {
        RedisSharePool p = new RedisSharePool();
        ShardedJedis jedis = p.getJedis();
        String s2 = jedis.get("ccc");
        System.out.println(s2);
        jedis.set("ccc", "eee");
        String s = jedis.get("ccc");
        System.out.println(s);
        jedis.close();
    }
}
