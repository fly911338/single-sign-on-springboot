package com.example.singlesignonspringboot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String username;
    private String password;
    private String permission;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
    @Transient
    private Set<GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        var authorities = new HashSet<SimpleGrantedAuthority>();
        Arrays.asList(permission.split(",")).forEach(authority -> authorities.add(new SimpleGrantedAuthority("ROLE_" + authority)));
        return authorities;
    }

    public static void main(String[] args) {
        var u = new Account();
        u.permission="ADMIN,USER";
        u.getAuthorities().forEach(System.out::println);
    }

}
